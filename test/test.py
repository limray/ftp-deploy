import os
import ftplib
import tempfile
import uuid
import shutil

from bitbucket_pipes_toolkit.test import PipeTestCase


class FTPDeployTestCase(PipeTestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.ftp_user = os.environ['FTP_USER']
        self.ftp_password = os.environ['FTP_PASSWORD']
        self.ftp_server = os.environ['FTP_SERVER']

    def setUp(self):
        self.tempdir = 'test-dir'
        self.filename = str(uuid.uuid4())
        os.mkdir(self.tempdir)
        with open(os.path.join(os.getcwd(), self.tempdir, self.filename), 'a'):
            pass

    def tearDown(self):
        shutil.rmtree(self.tempdir, ignore_errors=True)

    def test_ftp_deployment_successful(self):
        result = self.run_container(environment={
            'USER': self.ftp_user,
            'PASSWORD': self.ftp_password,
            'SERVER': self.ftp_server,
            'REMOTE_PATH': '/tmp',
            'BITBUCKET_CLONE_DIR': os.path.join(os.getcwd(), 'test')
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment finished.')

    def test_file_are_present_after_deploy(self):
        result = self.run_container(environment={
            'USER': self.ftp_user,
            'PASSWORD': self.ftp_password,
            'SERVER': self.ftp_server,
            'REMOTE_PATH': '/tmp',
            'LOCAL_PATH': os.path.join(os.getcwd(), self.tempdir)
        }, stderr=True)

        ftp = ftplib.FTP(host=self.ftp_server)
        ftp.login(user=self.ftp_user, passwd=self.ftp_password)
        files = ftp.nlst('tmp')


        self.assertIn(f'tmp/{os.path.basename(self.filename)}', files)
